﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Settings Flyout item template is documented at http://go.microsoft.com/fwlink/?LinkId=273769

namespace FishTank {
    public sealed partial class WindowsFIshTankSettings : SettingsFlyout {
        
        public Color BackgroundColor {
            get { return colorPicker.SelectedColor; }
            set { colorPicker.SelectedColor = value; }
        }

        public event EventHandler<Windows.UI.Color> BackgroundColorChanged;
        
        public WindowsFIshTankSettings() {
            this.InitializeComponent();
        }
        
        private void colorPicker_ColorSelected(object sender, Color e) {
            BackgroundColorChanged?.Invoke(this, e);
        }
    }
}
