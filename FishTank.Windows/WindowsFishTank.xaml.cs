﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Microsoft.Graphics.Canvas;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI;
using Microsoft.Graphics.Canvas.Numerics;
using Microsoft.Graphics.Canvas.Geometry;
using Microsoft.Graphics.Canvas.UI.Xaml;
using Windows.UI.ApplicationSettings;
using Windows.UI.Input;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FishTank {
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page {
        private WindowsFIshTankSettings fishtankSettings = new WindowsFIshTankSettings();
        FishController controller;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "Undeclared element")]
        public MainPage() {
            InitializeComponent();
            controller = new FishController(10, (int)Window.Current.Bounds.Width, (int)Window.Current.Bounds.Height, .95);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e) {
            Window.Current.SizeChanged += Current_SizeChanged;
            SettingsPane.GetForCurrentView().CommandsRequested += App_CommandsRequested;
            fishtankSettings.BackgroundColorChanged += FishtankSettings_ColorChanged;
        }

        private void FishtankSettings_ColorChanged(object sender, Color e) {
            mainCanvas.ClearColor = e;
        }

        private void Current_SizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e) {
            controller.ChangeSize((int)e.Size.Width, (int)e.Size.Height);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "Unused member")]
        private void mainCanvas_Draw(CanvasControl sender, CanvasDrawEventArgs args) {
            // Update the simulation state.

            controller.HandleUpdate(sender, args.DrawingSession);

            sender.Invalidate();
        }
        
        private void App_CommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args) {
            args.Request.ApplicationCommands.Add(new SettingsCommand("Fish Tank Settings", "Fish Tank Settings", (handler) => ShowSettingsFlyout()));
        }

        public void ShowSettingsFlyout() {
            fishtankSettings.BackgroundColor = mainCanvas.ClearColor;
            fishtankSettings.Show();
        }

        private void mainCanvas_PointerPressed(object sender, PointerRoutedEventArgs e) {
            PointerPoint point = e.GetCurrentPoint(mainCanvas);
            if (point.Properties.IsRightButtonPressed) {
                ShowSettingsFlyout();
            }
        }

        private void mainCanvas_Tapped(object sender, TappedRoutedEventArgs e) {
            var point = e.GetPosition(mainCanvas);
            controller.HandleClick(point.X, point.Y);
        }
    }
}
