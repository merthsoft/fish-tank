using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Graphics.Canvas;
using Windows.UI;
using Microsoft.Graphics.Canvas.Numerics;
using Microsoft.Graphics.Canvas.Geometry;
using Microsoft.Graphics.Canvas.UI.Xaml;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Shapes;
using static System.Math;

namespace FishTank {
    public class Fish {
        public FishController Controller { get; set; }
        
        public Vector2 Location { get; set; }
        public Vector2 Size { get; set; }
        public Color Color { get; set; }
        public Vector2 Speed { get; set; }
        public FishDirection Direction { get; set; }

        public bool ShouldRemove {  get { return ((Location.X - (Size.X + Size.X / 4)) > Controller.ScreenWidth) || (Location.X < -(Size.X + Size.X / 4)); } }
        
        public Fish(FishController controller, float x, float y, float width, float height, Color color, float speedX, float speedY, FishDirection direction) {
            Controller = controller;
            Location = new Vector2() { X = x, Y = y };
            Size = new Vector2() { X = width, Y = height };
            Color = color;
            Speed = new Vector2() { X = speedX, Y = speedY};
            Direction = direction;
        }

        public static Fish RandomFish(FishController controller) {
            int width = controller.Random.Next(20, 100);
            int height = controller.Random.Next(20, 100);
            int y = controller.Random.Next(height / 2, controller.ScreenHeight - height / 2);
            FishDirection d = (FishDirection)controller.Random.Next(0, 2);
            int x = d == FishDirection.Right ? -width : (controller.ScreenWidth + width);
            float speedX = (float)(7 * controller.Random.NextDouble() + .5);
            float speedY = (float)(2.0 * (.5 - controller.Random.NextDouble()));
            return new Fish(controller, x, y, width, height, randomColor(controller), speedX, speedY, d);
        }

        private static Color randomColor(FishController controller) {
            byte[] c = new byte[3];
            controller.Random.NextBytes(c);
            Color newColor = new Color() { R = c[0], G = c[1], B = c[2], A = 255 };
            return newColor;
        }

        public void ClickFish() {
            switch (Direction) {
                case FishDirection.Left:
                    Direction = FishDirection.Right;
                    break;
                case FishDirection.Right:
                    Direction = FishDirection.Left;
                    break;
            }

            Speed = new Vector2() { X = 25, Y = Speed.Y };
        }

        public bool PointIntersects(double x, double y) {
            double h = Location.X;
            double k = Location.Y;
            double rx = Size.X;
            double ry = Size.Y;

            double val = Square(x - h) / Square(rx) + Square(y - k) / Square(ry);
            return val <= 1;
        }

        private static Double Square(double val) {
            return Pow(val, 2);
        }

        public void Draw(CanvasControl control, CanvasDrawingSession canvas) {
            canvas.FillEllipse(Location, Size.X, Size.Y, Color);
            drawEye(canvas);
            drawMouth(canvas);
            drawTail(control, canvas);
        }

        private void drawTail(CanvasControl control, CanvasDrawingSession canvas) {
            CanvasPathBuilder path = new CanvasPathBuilder(control.Device);

            switch (Direction) {
                case FishDirection.Left:
                    path.BeginFigure(Location.X + Size.X - 5, Location.Y);
                    path.AddLine(Location.X + Size.X + Size.X / 4, Location.Y - Size.Y / 2);
                    path.AddLine(Location.X + Size.X + Size.X / 4, Location.Y + Size.Y / 2);
                    break;
                default:
                case FishDirection.Right:
                    path.BeginFigure(Location.X - Size.X + 5, Location.Y);
                    path.AddLine(Location.X - Size.X - Size.X / 4, Location.Y - Size.Y / 2);
                    path.AddLine(Location.X - Size.X - Size.X / 4, Location.Y + Size.Y / 2);
                    break;
            }
            path.EndFigure(CanvasFigureLoop.Closed);

            canvas.FillGeometry(CanvasGeometry.CreatePath(path), Color);
        }

        private void drawEye(CanvasDrawingSession canvas) {
            Vector2 eyeLocation;
            switch (Direction) {
                case FishDirection.Left:
                    eyeLocation = new Vector2() {
                        X = Location.X - Size.X + Size.X / 4,
                        Y = Location.Y - 5
                    };
                    break;
                default:
                case FishDirection.Right:
                    eyeLocation = new Vector2() {
                        X = Location.X + Size.X - Size.X / 4,
                        Y = Location.Y - 5
                    };
                    break;
            }

            canvas.FillCircle(eyeLocation, 5, Colors.White);
            canvas.FillCircle(eyeLocation, 2, Colors.Black);
        }

        private void drawMouth(CanvasDrawingSession canvas) {
            Vector2 mouthPoint1;
            Vector2 mouthPoint2;

            switch (Direction) {
                case FishDirection.Left:
                    mouthPoint1 = new Vector2() {
                        X = Location.X - Size.X,
                        Y = Location.Y
                    };
                    mouthPoint2 = new Vector2() {
                        X = Location.X - Size.X + Size.X / 4 - 10,
                        Y = Location.Y
                    };
                    break;
                default:
                case FishDirection.Right:
                    mouthPoint1 = new Vector2() {
                        X = Location.X + Size.X,
                        Y = Location.Y
                    };
                    mouthPoint2 = new Vector2() {
                        X = Location.X + Size.X - Size.X / 4 + 10,
                        Y = Location.Y
                    };
                    break;
            }

            canvas.DrawLine(mouthPoint1, mouthPoint2, Colors.Black);
        }

        public void Step() {
            switch (Direction) {
                case FishDirection.Left:
                    Location = new Vector2() { X = Location.X - Speed.X, Y = Location.Y + Speed.Y };
                    break;
                case FishDirection.Right:
                    Location = new Vector2() { X = Location.X + Speed.X, Y = Location.Y + Speed.Y };
                    break;
                default:
                    break;
            }

            //if (Random.NextDouble() < .1) {
            //    Speed.Y = (float)(2.0 * (.5 - Random.NextDouble()));
            //}

            //if (Random.NextDouble() < .1) {
            //    Speed.X = (float)(7 * Random.NextDouble() + .5);
            //}
        }
    }
}
