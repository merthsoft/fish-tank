﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Numerics;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Windows.UI;

namespace FishTank {
    public class FishController {
        public Random Random { get; private set; }
        public int ScreenWidth { get; private set; }
        public int ScreenHeight { get; private set; }

        Stopwatch timer;
        TimeSpan lastTime;

        List<Fish> fishies = new List<Fish>();

        double randChance;

        public FishController(int numberOfInitialFish, int width, int height, double randChance) {
            Random =  new Random();
            timer = Stopwatch.StartNew();

            ScreenWidth = width;
            ScreenHeight = height;
            
            for (int i = 0; i < numberOfInitialFish; i++) {
                fishies.Add(Fish.RandomFish(this));
            }

            this.randChance = randChance;
        }

        public void ChangeSize(int newWidth, int newHeight) {
            int oldWidth = ScreenWidth;
            int oldHeight = ScreenHeight;

            foreach (var f in fishies) {
                f.Location = new Vector2() {
                    X = (int)Math.Round(f.Location.X * ((double)newWidth / oldWidth)),
                    Y = (int)Math.Round(f.Location.Y * ((double)newHeight / oldHeight)),
                };
            }

            ScreenWidth = newWidth;
            ScreenHeight = newHeight;
        }

        public void HandleClick(double x, double y) {
            Fish clickedFish = fishies.FindLast(f => f.PointIntersects(x, y));
            clickedFish?.ClickFish();
        }

        public void HandleUpdate(CanvasControl control, CanvasDrawingSession canvas) {
            var currentTime = timer.Elapsed;
            var elapsed = (currentTime - lastTime).TotalSeconds;

            if (elapsed >= 1f / 30) {
                step();
                lastTime = currentTime;
            }


            // Display the current surface.
            drawFish(control, canvas);
        }

        private void step() {
            foreach (var fish in fishies) {
                fish.Step();
            }
            fishies.RemoveAll(f => f.ShouldRemove);

            if (Random.NextDouble() > randChance) {
                fishies.Insert(Random.Next(fishies.Count), Fish.RandomFish(this));
            }
        }

        private void drawFish(CanvasControl control, CanvasDrawingSession canvas) {
            foreach (var fish in fishies) {
                fish.Draw(control, canvas);
            }
        }
    }
}
