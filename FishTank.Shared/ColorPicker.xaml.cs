﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FishTank {
    public sealed partial class ColorPicker : UserControl {
        public event EventHandler<Color> ColorSelected;

        public Color SelectedColor {
            get {
                return HSVToRGB(selectedH, selectedS, selectedV);
            }
            set {
                selectedH = 0;
            }
        }
        
        double selectedH;
        double selectedS;
        double selectedV;
        private bool mouseIsDownInColorWheel;
        private bool mouseIsDownInSlider;

        public ColorPicker() {
            this.InitializeComponent();
        }

        private void colorWheel_Draw(Microsoft.Graphics.Canvas.UI.Xaml.CanvasControl sender, Microsoft.Graphics.Canvas.UI.Xaml.CanvasDrawEventArgs args) {
            double size = Math.Min(colorWheel.ActualWidth, colorWheel.ActualHeight);

            for (int x = 0; x < size; x++) {
                for (int y = 0; y < size; y++) {
                    var color = HSVToRGB(x/size, y / size, 1.0);
                    args.DrawingSession.FillRectangle(x, y, 1, 1, color);
                }
            }
        }

        private void slider_Draw(Microsoft.Graphics.Canvas.UI.Xaml.CanvasControl sender, Microsoft.Graphics.Canvas.UI.Xaml.CanvasDrawEventArgs args) {
            double width = slider.ActualWidth;
            int height = (int)slider.ActualHeight;

            for (int x = 0; x < width; x++) {
                var color = HSVToRGB(selectedH, selectedS, x / width);
                args.DrawingSession.FillRectangle(x, 0, 1, height, color);
            }
        }

        private void handleColorWheelMouse(PointerRoutedEventArgs e) {
            double size = Math.Min(colorWheel.ActualWidth, colorWheel.ActualHeight);
            var point = e.GetCurrentPoint(colorWheel);
            if (point.Position.X > size || point.Position.X < 0 || point.Position.Y > size || point.Position.Y < 0) { return; }

            selectedH = point.Position.X / size;
            selectedS = point.Position.Y / size;
            selectedV = 1.0;
            slider.Invalidate();

            ColorSelected?.Invoke(this, SelectedColor);
        }

        private void colorWheel_PointerPressed(object sender, PointerRoutedEventArgs e) {
            mouseIsDownInColorWheel = true;
            handleColorWheelMouse(e);
        }

        private void colorWheel_PointerMoved(object sender, PointerRoutedEventArgs e) {
            if (mouseIsDownInColorWheel) {
                handleColorWheelMouse(e);
            }
        }

        private void colorWheel_PointerReleased(object sender, PointerRoutedEventArgs e) {
            mouseIsDownInColorWheel = false;
        }

        private void slider_PointerPressed(object sender, PointerRoutedEventArgs e) {
            mouseIsDownInSlider = true;
            handleSliderMouse(e);
        }

        private void slider_PointerMoved(object sender, PointerRoutedEventArgs e) {
            if (mouseIsDownInSlider) { handleSliderMouse(e); }
        }

        private void slider_PointerReleased(object sender, PointerRoutedEventArgs e) {
            mouseIsDownInSlider = false;
        }

        private void handleSliderMouse(PointerRoutedEventArgs e) {
            double size = slider.ActualWidth;
            var point = e.GetCurrentPoint(colorWheel);
            
            selectedV = point.Position.X / size;
            ColorSelected?.Invoke(this, SelectedColor);
        }

        // http://viziblr.com/news/2011/12/1/drawing-a-color-hue-wheel-with-c.html
        public static Color HSVToRGB(double H, double S, double V) {
            double R, G, B;
            if (H == 1.0) {
                H = 0.0;
            }

            double step = 1.0 / 6.0;
            double vh = H / step;

            int i = (int)System.Math.Floor(vh);

            double f = vh - i;
            double p = V * (1.0 - S);
            double q = V * (1.0 - (S * f));
            double t = V * (1.0 - (S * (1.0 - f)));

            switch (i) {
                case 0:
                    R = V;
                    G = t;
                    B = p;
                    break;
                case 1:
                    R = q;
                    G = V;
                    B = p;
                    break;
                case 2:
                    R = p;
                    G = V;
                    B = t;
                    break;
                case 3:
                    R = p;
                    G = q;
                    B = V;
                    break;
                case 4:
                    R = t;
                    G = p;
                    B = V;
                    break;
                case 5:
                    R = V;
                    G = p;
                    B = q;
                    break;
                default:
                    // not possible - if we get here it is an internal error
                    throw new ArgumentException();
            }


            return Color.FromArgb((byte)255, (byte)(R * 255), (byte)(G * 255), (byte)(B * 255));
        }
    }
}
